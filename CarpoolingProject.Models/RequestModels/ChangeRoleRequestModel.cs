﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarpoolingProject.Models.RequestModels
{
    public class ChangeRoleRequestModel
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
