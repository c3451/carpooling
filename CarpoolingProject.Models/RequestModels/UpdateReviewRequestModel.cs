﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarpoolingProject.Models.RequestModels
{
    public class UpdateReviewRequestModel:ReviewRequestModel
    {
        public int ReviewId { get; set; }
    }
}
