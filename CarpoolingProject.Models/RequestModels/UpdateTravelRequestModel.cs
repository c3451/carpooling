﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarpoolingProject.Models.RequestModels
{
    public class UpdateTravelRequestModel:CreateTravelRequestModel
    {
        public int TravelId { get; set; }
        [Required]
        public string ConfirmPassword { get; set; }
    }
}
