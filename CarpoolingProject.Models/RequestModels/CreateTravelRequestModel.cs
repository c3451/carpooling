﻿using CarpoolingProject.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarpoolingProject.Models.RequestModels
{
    public class CreateTravelRequestModel
    {
        public int CreatorId { get; set; }
        public User Creator { get; set; }
        public string StartCity { get; set; }
        public string StartStreet { get; set; }
        public string EndCity { get; set; }
        public string EndStreet { get; set; }
        public DateTime DepartureTime { get; set; }//Plovdiv,ulicata
        public int FreeSpots { get; set; }
        public double PricePerPerson { get; set; }
        public string AdditionalInfo { get; set; }
    }
}
