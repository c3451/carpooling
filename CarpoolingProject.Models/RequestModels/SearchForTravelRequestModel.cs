﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarpoolingProject.Models.RequestModels
{
    public class SearchForTravelRequestModel
    {
        public string StartCity { get; set; }
        public string EndCity { get; set; }
        public DateTime Date { get; set; }
        
    }
}
