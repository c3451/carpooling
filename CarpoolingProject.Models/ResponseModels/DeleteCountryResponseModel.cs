﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarpoolingProject.Models.ResponseModels
{
    public class DeleteCountryResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
