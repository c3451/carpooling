﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarpoolingProject.Models.Dtos
{
    public class ApplicantsDto:UserDto
    {
        public ApplicantsDto()
        {

        }
        public int TravelId { get; set; }
        public string StartPoint { get; set; }
        public string EndPoint { get; set; }
    }
}
