﻿using CarpoolingProject.Models.EntityModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarpoolingProject.Data
{
    public static class ModelBuilderExtension
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            
            var cities = new List<City>
            {
                new City
                {
                    CityId=1,
                    Name="Varna",
                    CountryId=1
                }
            };
            var countries = new List<Country>
            {
                new Country
                {
                    CountryId=1,
                    Name="Bulgaria"
                }
            };
            var addresses = new List<Address>
            {
                new Address
                {
                    AddressId=1,
                    StreetName="ulica selo",
                    CityId=1
                }
            };
            var roles = new List<Role>
            {
                new Role
                {
                    RoleId = 1,
                    Name = "Admin"

                },
                new Role
                {
                    RoleId = 2,
                    Name = "Driver"
                },
                new Role
                {
                    RoleId = 3,
                    Name = "Passanger"
                },
                new Role
                {
                    RoleId = 4,
                    Name = "Blocked"
                }
            };
            var userRoles = new List<UserRole>
            {
                new UserRole
                {
                    Id=1,
                    RoleId=1,
                    UserId=1
                },
                new UserRole
                {
                    Id=2,
                    RoleId=2,
                    UserId=2
                }
            };
            var users = new List<User>
            {
                new User
                {
                    UserId=1,
                    UserName="PeshoSalama",
                    PasswordHash="bezparola",
                    FirstName="Pesho",
                    LastName="Salama",
                    Email="pesho.salama@gmail.com",
                    PhoneNumber="088865432",
                    TravelCountAsDriver=0,
                    StarsCount=0,
                    ReviewCount=0

                },
                new User
                {
                    UserId=2,
                    UserName="Customer",
                    PasswordHash="bezparola",
                    FirstName="Foncho",
                    LastName="Tarikata",
                    Email="foncho.tarikata@gmail.com",
                    PhoneNumber="088863432",
                    TravelCountAsDriver=0,
                    StarsCount=0,
                    ReviewCount=0

                },
                new User
                {
                    UserId=3,
                    UserName="Shafior",
                    PasswordHash="bezparola",
                    FirstName="Lyubo",
                    LastName="Grigorov",
                    Email="lyubo.grigorov@gmail.com",
                    PhoneNumber="088845432",
                    TravelCountAsDriver=0,
                    StarsCount=0,
                    ReviewCount=0

                }
            };
            //var travels = new List<Travel>
            //{
            //    new Travel
            //    {
            //        TravelId=1,
            //        CreatorId=1,
            //        Creator= users.FirstOrDefault(x=>x.UserId==1),
            //        StartPointId=1,
            //        EndPointId=1,
            //        DepartureTime= new DateTime(2021, 11, 15, 22, 50,00),
            //        FreeSpots= 4
            //    },
            //    new Travel
            //    {
            //        TravelId=2,
            //        CreatorId=2,
            //        Creator= users.FirstOrDefault(x=>x.UserId==2),
            //       StartPointId=1,
            //        EndPointId=1,
            //        DepartureTime= new DateTime(2021, 11, 15, 22, 50,00),
            //        FreeSpots= 2
            //    },
            //     new Travel
            //    {
                    
            //        TravelId=3,
            //        CreatorId=3,
            //        Creator= users.FirstOrDefault(x=>x.UserId==3),
            //        StartPointId=1,
            //        EndPointId=1,
            //        DepartureTime= new DateTime(2021, 11, 15, 22, 50,00),
            //        FreeSpots= 3
            //    }
            //};
           

            modelBuilder.Entity<Country>().HasData(countries);
            modelBuilder.Entity<City>().HasData(cities);
            modelBuilder.Entity<Address>().HasData(addresses);
            modelBuilder.Entity<Role>().HasData(roles);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<UserRole>().HasData(userRoles);
           // modelBuilder.Entity<Travel>().HasData(travels);

        }
    }
}
