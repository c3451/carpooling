﻿using AutoMapper;
using CarpoolingProject.Models.EntityModels;
using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Services.Interfaces;
using CarpoolingProject.Web.Helper;
using CarpoolingProject.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace CarpoolingProject.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService userService;
        private readonly IMapper mapper;
        private readonly ITravelService travelService;
        private readonly IAuthHelper authHelper;
        private readonly IMailService mailService;

        public UserController(IUserService userService, IMapper mapper, IAuthHelper authHelper, IMailService mailService)
        {
            this.userService = userService;
            this.mapper = mapper;
            this.travelService = travelService;
            this.authHelper = authHelper;
            this.mailService = mailService;
        }

        //GET: /auth/login
        public IActionResult Login()
        {
            var loginViewModel = new LoginViewModel();

            return this.View(loginViewModel);
        }

        //POST: /auth/login
        [HttpPost]
        public async Task<IActionResult> Login([Bind("Username, Password")] LoginViewModel loginViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(loginViewModel);
            }

            try
            {
                var user = await authHelper.TryGetUserAsync(loginViewModel.Username, loginViewModel.Password);
                this.HttpContext.Session.SetString("CurrentUser", user.UserName);
                this.HttpContext.Session.SetString("CurrentRoles", string.Join(',', user.Roles.Select(userRole => userRole.Role.Name)));

                return this.RedirectToAction("index", "home");
            }

            catch (AuthenticationException e)
            {
                this.ModelState.AddModelError("Email", e.Message);
                return this.View(loginViewModel);
            }
        }

        //GET: /auth/logout
        public IActionResult Logout()
        {
            this.HttpContext.Session.Remove("CurrentUser");

            return this.RedirectToAction("index", "home");
        }

        //GET: /auth/register
        public IActionResult Register()
        {
            var registerRequestModel = new CreateUserRequestModel();

            return this.View(registerRequestModel);
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromForm] CreateUserRequestModel requestModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(requestModel);
            }

            if (this.userService.Exist(requestModel.Email))
            {
                this.ModelState.AddModelError("Username", "User with same email already exists.");
                return this.View(requestModel);
            }

            //if (!registerViewModel.Password.Equals(registerViewModel.ConfirmPassword))
            //{
            //    this.ModelState.AddModelError("ConfirmPassword", "Confirm password should match password.");
            //    return this.View(registerViewModel);
            //}          

            await userService.CreateUserAsync(requestModel);
            var userName = requestModel.UserName;
            await mailService.MailVerificationAsync(userName);

            return this.RedirectToAction("Login");
        }

        [HttpPut]
        public async Task<IActionResult> Update(UserViewModel user)
        {
            var userName = HttpContext.Session.GetString("CurrentUser");
            var userToUpdate = await userService.GetUsernameAsync(userName);

            var updateUser = mapper.Map<UpdateUserRequestModel>(user);
            user.UserId = userToUpdate.UserId;

            if (user.ImageFile != null)
            {
                await ExtensionMethod.UploadImageFile(user.UserId, "user", user.ImageFile, false);
                updateUser.ImagePath = $"/images/users/{user.UserId}.jpg";
            }
            await userService.UpdateUserAsync(updateUser);
            return this.View(user);
        }
        public async Task<IActionResult> MyTravels()
        {
            var username = HttpContext.Session.GetString("CurrentUser");
            var user = await userService.GetUsernameAsync(username);

            var travels = await travelService.GetAllTravelsAsync(user.UserId);
            return this.View("MyTravels_TableView", travels);
        }
        public async Task<IActionResult> FutureTravels()
        {
            var username = HttpContext.Session.GetString("CurrentUser");
            var user = await userService.GetUsernameAsync(username);

            var travels = await travelService.FutureTravelsASync(user.UserId);
            return this.View("MyTravels_TableView", travels);
        }
        public async Task<IActionResult> Finish(int id)
        {
            var travel = await travelService.GetTravelAsync(id);
            return RedirectToAction("FutureTravels");
        }

    }
}
