﻿using CarpoolingProject.Models.EntityModels;
using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolingProject.Web.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IAdminService adminService;

        public AdminController(IAdminService adminService)
        {
            this.adminService = adminService;
        }

        [HttpPut("role")]
        public async Task<IActionResult> ChangerRole(ChangeRoleRequestModel requestModel)
        {
            var result = await adminService.ChangeRoleAsync(requestModel);
            return this.Ok(result);
        }

        [HttpDelete("")]
        public async Task<IActionResult> Delete([FromBody] DeleteUserRequestModel requestModel)
        {
            var responce = await adminService.DeleteUserAsync(requestModel);
            if (responce.IsSuccess)
            {
                return this.Ok(responce.Message);
            }
            return BadRequest(responce.Message);
        }

        [HttpGet("Drivers")]
        public async Task<IActionResult> GetDrivers()
        {
            var drivers = await adminService.GetAllDriversAsync();
            return Ok(drivers);
        }

        [HttpGet("Travels")]
        public async Task<IActionResult> GetTravels()
        {
            var travels = await adminService.GetAllTravelsAsync();
            return Ok(travels);
        }

        [HttpGet("Passengers")]
        public async Task<IActionResult> GetPassengers()
        {
            var users = await adminService.GetAllPassengersAsync();
            return Ok(users);
        }

        [HttpPut("users/{id}/block")]
        public async Task<IActionResult> Block(int id)
        {
            var response = await adminService.BlockAsync(id);

            return this.Ok($"User with Id:{id} was blocked!");
        }

        [HttpPut("users/{id}/unblock")]
        public async Task<IActionResult> UnBlock(int id)
        {
            var response = await adminService.UnBlockAsync(id);

            return this.Ok($"User with Id:{id} was unblocked!");
        }
    }
}
